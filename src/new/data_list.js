

main_controllers.controller('data_list_controller', function($http, $scope, $rootScope, $location, $routeParams) {
  $rootScope.activemenu = "data_list_controller";
  
    $scope.ui= {};
    $scope.pg= {};
    $scope.rs = $rootScope;
    $scope.list_result = {
      records :{
        data:[]
      }
    };

  $scope.collection_id = 's1';
  $scope.collection_list = function (formvalid) {
    var payload = {};
    payload.products = true;

    var mypromise =  $http({
      method: 'POST',
      url: $rootScope.api_hymne + '/collection_list',
      params: { 'foobar': new Date().getTime() },
      data: payload
    });
    mypromise.success(function(data, status, headers, config) {
      if (status == 200) {
        $scope.lookup_collections = data;
        console.log(JSON.stringify($scope.lookup_collections));
      } else {
        http_error(data);
      }
    });
    mypromise.error(function(data, status, headers, config) {
      http_error(data);
    });
    function http_error(data){
      if (data.message) {
        alert(data.message);
      } else {
        alert('Unknown error: ' + data);
      }
    }
  };
  $scope.collection_list();

    // Refine search defaults
    $scope.bi = {};
    $scope.bi.mineral_size_type = '';
    $scope.bi.categories = '';
    $scope.bi.type_sale = '';
    $scope.bi.sort_by = 'Alphabetically';
    $scope.bi.search_string = '';

    //Refine your search
    $scope.ui.search = 'false';
    $scope.hideButton = function() {  
    $scope.ui.search = 'false';
    };
    $scope.showButton = function() {  
    $scope.ui.search = 'true';
    };
    $scope.showme = true;

// pagenation
  $scope.pg.page = 1;
  $scope.pg.pages = 1;
  $scope.pg.page_size = 20;

  if($location.search().page){
    var pagestr = $location.search().page;
    if(isInt(pagestr)){
      $scope.pg.page = parseInt(pagestr,10);
    }
  }

  function calcpages1(){
    $scope.pg.available = $scope.list_result.records.available;
    var mod_remainder = $scope.pg.available % $scope.pg.page_size;
    $scope.pg.pages = ($scope.pg.available - mod_remainder) / $scope.pg.page_size;
    if(mod_remainder > 0){
      $scope.pg.pages = $scope.pg.pages + 1;
    }
    $scope.pg.page = Math.floor($scope.list_result.records.offset / $scope.pg.page_size) + 1;
  }

    $scope.ui.product_list_merchant = [];

    $scope.page_button = function(page){
      $scope.pg.page = page;
      $scope.product_list_merchant();
    };

    $scope.product_list_merchant2 = function () {
      $scope.pg.page = 1;
      $scope.pg.page_size = 20;
      $scope.product_list_merchant();
    }


    $scope.product_list_merchant = function () {
        var payload = {};
        payload.collection_id = $scope.collection_id;
        payload.offset = ($scope.pg.page - 1) * $scope.pg.page_size;
        payload.limit = $scope.pg.page_size;
        var mypromise =  $http({
            method: 'POST',
            url: $scope.rs.api_hymne + '/product_list_merchant/10',
            params: { 'foobar': new Date().getTime() },
            data: payload
          });
          mypromise.success(function(data, status, headers, config) {
            if (status == 200) {
              $scope.list_result = data;
              $scope.ui.product_list_merchant = data.records.data;
              calcpages1();
            } else {
              http_error(data);
            }
          });
          mypromise.error(function(data, status, headers, config) {
            http_error(data);
          });
          function http_error(data){
            if (data.message) {
              alert(data.message);
            } else {
              alert('Unknown error: ' + data);
            }
          }
    };
  // $scope.product_list_merchant();

  $scope.image_uri = function(file_name){
       return $scope.rs.api_hymne_files100 + "/" + file_name;
   };

  var grid_open_template = '<div style="text-align:center">';
  grid_open_template += '<button type="button" class="btn btn-secondary btn-xs" ng-click="view_product(row.entity[col.field])">';
  grid_open_template += '<i class="fa fa-arrow-right fa-3x"></i>';
  grid_open_template += '</button>';
  grid_open_template += '</div>';

  var grid_edit_template = '<div style="text-align:center">';
  grid_edit_template += '<button type="button" class="btn btn-secondary" ng-click="edit_product(row.entity[col.field])">';
  grid_edit_template += '<font color="black"> <i class="fa fa-pencil-square-o fa-3x"></i> </font>';
  grid_edit_template += '</button>';
  grid_edit_template += '</div>';

  var grid_delete_template = '<div style="text-align:center">';
  grid_delete_template += '<button type="button" class="btn btn-secondary btn-xs" ng-click="delete_product(row.entity[col.field])">';
  grid_delete_template += '<font color="black"> <i class="fa fa-trash-o fa-3x"></i> </font>';
  grid_delete_template += '</button>';
  grid_delete_template += '</div>';

  var grid_img_template = '<a ng-href="#/product/{{row.getProperty(\'_id\')}}"><span ng-show="row.getProperty(col.field)">';
  grid_img_template += '<img ng-src="{{image_uri(row.entity[col.field])}}" style="max-height:50px"/>';
  grid_img_template += '</span><a>';

  var myDateTemplate = '<input ui-date="{ dateFormat: \'dd mm yyyy\' }" ui-date-format="dd mm yyyy" ng-model="row.entity.myDate"/>';
 
  $scope.view_product = function(pid){
    console.log(pid);
    $location.path('/data_view/' + pid );
  };

   $scope.edit_product = function(pid){
    console.log(pid);
    $location.path('/data_edit/' + pid );
  };


  $scope.grid_cols_def = [
    {field: 'main_pic', displayName: 'Picture', width: 80, cellTemplate : grid_img_template, editableCellTemplate : grid_img_template },
    {field: '_id', displayName: 'Edit', width: 80, cellTemplate : grid_edit_template, editableCellTemplate : grid_edit_template },
    {field: 'product_no', displayName: 'Product #', width: 80},
    {field: 'title', displayName: 'Title', width: 350},
    {field: 'mineral_size_type', displayName: 'Size Type', width: 120},
    {field: 'price', displayName: 'Price', width: 80},
    {field: 'capture_time', displayName:'Capture Time', cellFilter: 'date:\'dd MMMM yyyy -@- HH:MM:ss \'' },

  ];
  $scope.grid_filter_options = {
    filterText: ''
  };


  $scope.grid_options = {
    rowHeight: 50,
    data: 'ui.product_list_merchant',
    enableCellSelection: false,
    enableRowSelection: false,
    enableCellEditOnFocus: false,
    columnDefs: 'grid_cols_def',
    enablePaging: true,
    filterOptions: $scope.grid_filter_options
  };


  $scope.product_list_merchant();

  $scope.delete_product = function(pid){
     var payload = {};
      payload.deleted = true;
      payload.id = pid;
      var mypromise =  $http({
        method: 'POST',
        url: $rootScope.api_hymne + '/product_update/' + pid,    //map to API server.js: app.delete('/articles/:id', articles.articles_delete_one);
        params: {'foobar': new Date().getTime()},
        data: payload
      });
      mypromise.success(function(data, status, headers, config) {
        if (status == 200) {
          $scope.product_list_merchant();       // Recalls the ui_get_articles_button() function to reload the list of articles in the grid
        } else {
          http_error(data);
        }
      });
      mypromise.error(function(data, status, headers, config) {
        http_error(data);
      });
      function http_error(data){
          if (data.message) {
            alert(data.message);
          } else {
            alert('Unknown error: ' + data);
          }
      }

  };

  $scope.ui_reset_filter_button = function () {

    $scope.ui.museum = '';
    $scope.ui.cabinet = '';
    $scope.ui.wholesale = '';
    $scope.ui.thumbnail = '';
    $scope.ui.small_cabinet = '';
    $scope.ui.miniature = '';

    $scope.ui.chwaning_wessels_hatazel = '';
    $scope.ui.rest_sa = '';
    $scope.ui.brandberg_erongo_okorusu = '';
    $scope.ui.world_selection = '';
    $scope.ui.sa_and_world_mineral_books = '';
    $scope.ui.okandawasi_koakoland = '';
    $scope.ui.rosh_pinah_skorpion = '';
    $scope.ui.orange_river_riemvasmaak = '';
    $scope.ui.wholesale_lots = '';
    $scope.ui.colin_collection = '';
    $scope.ui.sugilite_wessels = '';
    $scope.ui.tsumeb_kombat_aukas = '';
    $scope.ui.pictures_mines = '';
    $scope.ui.meteorites = '';
    $scope.ui.musina_zimbabwe_zambia = '';

    $scope.ui.part_of_auction = '';
    $scope.ui.shop_items = '';
    $scope.ui.exeptional = '';
    $scope.ui.fluorescent = '';
    $scope.ui.fluorescent_colour = '';

  };

  $scope.ui_get_filter_button = function () {
        var payload = {};
        payload.search_string = $scope.bi.search_string;
        payload.sort_by = $scope.bi.sort_by;

        payload.museum = $scope.ui.museum;
        payload.cabinet = $scope.ui.cabinet;
        payload.wholesale = $scope.ui.wholesale;
        payload.thumbnail = $scope.ui.thumbnail;
        payload.small_cabinet = $scope.ui.small_cabinet;
        payload.miniature = $scope.ui.miniature;

        payload.chwaning_wessels_hatazel = $scope.ui.chwaning_wessels_hatazel;
        payload.rest_sa = $scope.ui.rest_sa;
        payload.brandberg_erongo_okorusu = $scope.ui.brandberg_erongo_okorusu;
        payload.world_selection = $scope.ui.world_selection;
        payload.sa_and_world_mineral_books = $scope.ui.sa_and_world_mineral_books;
        payload.okandawasi_koakoland = $scope.ui.okandawasi_koakoland;
        payload.rosh_pinah_skorpion = $scope.ui.rosh_pinah_skorpion;
        payload.orange_river_riemvasmaak = $scope.ui.orange_river_riemvasmaak;
        payload.wholesale_lots = $scope.ui.wholesale_lots;
        payload.colin_collection = $scope.ui.colin_collection;
        payload.sugilite_wessels = $scope.ui.sugilite_wessels;
        payload.tsumeb_kombat_aukas = $scope.ui.tsumeb_kombat_aukas;
        payload.pictures_mines = $scope.ui.pictures_mines;
        payload.meteorites = $scope.ui.meteorites;
        payload.musina_zimbabwe_zambia = $scope.ui.musina_zimbabwe_zambia;

        payload.part_of_auction = $scope.ui.part_of_auction;
        payload.shop_items = $scope.ui.shop_items;
        payload.exeptional = $scope.ui.exeptional;
        payload.fluorescent = $scope.ui.fluorescent;
        payload.fluorescent_colour = $scope.ui.fluorescent_colour;

        payload.id = $scope.rs.session.profile_id;
        payload.offset = ($scope.pg.page - 1) * $scope.pg.page_size;
        payload.limit = $scope.pg.page_size;
        console.log($scope.rs.session.profile_id);
        var mypromise =  $http({
            method: 'POST',
            url: $scope.rs.api_hymne + '/product_list_filtered/',
            params: { 'foobar': new Date().getTime() },
            data: payload
          });
          mypromise.success(function(data, status, headers, config) {
            if (status == 200) {
              $scope.list_result = data;
              $scope.ui.product_list_merchant = data.records.data;
              calcpages1();

              console.log($scope.ui.product_list_merchant[0]);
            } else {
              http_error(data);
            }
          });
          mypromise.error(function(data, status, headers, config) {
            http_error(data);
          });
          function http_error(data){
            if (data.message) {
              alert(data.message);
            } else {
              alert('Unknown error: ' + data);
            }
          }
    };


  $scope.ui_get_product_string_button = function () {
        var payload = {};
          payload.search_string = $scope.bi.search_string;
          payload.sort_by = $scope.bi.sort_by;
        
        var mypromise =  $http({
            method: 'POST',
            url: $scope.rs.api_hymne + '/product_list_search_string/',
            params: { 'foobar': new Date().getTime() },
            data: payload
          });
          mypromise.success(function(data, status, headers, config) {
            if (status == 200) {
              $scope.list_result = data;
              $scope.ui.product_list_merchant = data.records.data;
              calcpages1();

              console.log($scope.ui.product_list_merchant[0]);
            } else {
              http_error(data);
            }
          });
          mypromise.error(function(data, status, headers, config) {
            http_error(data);
          });
          function http_error(data){
            if (data.message) {
              alert(data.message);
            } else {
              alert('Unknown error: ' + data);
            }
          }
    };


});










