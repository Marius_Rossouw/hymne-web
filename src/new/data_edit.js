main_controllers.controller('data_edit_controller', function($http, $scope, $rootScope, $location, $routeParams, modal_service, $modal) {

  $scope.ui= {
    features : "",
    item_type_sale : "",
    images : [],
    type_sale : 'Undecided',
    mineral_size_type : '',
    categories : '',
    collection_id:'564ccc4144ae7d2b0ac14bcf',
    special_class: '',
    special_characteristics : '',
    largest_d : 0,
    type : 'Minerals'
  };
  $scope.lookup_collections = [];

  $scope.rs = $rootScope;

  // $scope.collection_list = function (formvalid) {
  //   var payload = {};
  //   payload.products = true;

  //   var mypromise =  $http({
  //     method: 'POST',
  //     url: $rootScope.api_hymne + '/collection_list',
  //     params: { 'foobar': new Date().getTime() },
  //     data: payload
  //   });
  //   mypromise.success(function(data, status, headers, config) {
  //     if (status == 200) {
  //       $scope.lookup_collections = data;
  //     } else {
  //       http_error(data);
  //     }
  //   });
  //   mypromise.error(function(data, status, headers, config) {
  //     http_error(data);
  //   });
  //   function http_error(data){
  //     if (data.message) {
  //       alert(data.message);
  //     } else {
  //       alert('Unknown error: ' + data);
  //     }
  //   }
  // };
  // $scope.collection_list();

  $scope.product_getone = function (formvalid) {
    if (!$routeParams.id || $routeParams.id.length == 0 || $routeParams.id == 'new'){
      return;
    }
    var payload = {};

    var mypromise =  $http({
      method: 'POST',
      url: $rootScope.api_hymne + '/product_getone/' + $routeParams.id,
      params: { 'foobar': new Date().getTime() },
      data: payload
    });
    mypromise.success(function(data, status, headers, config) {
      if (status == 200) {
        $scope.ui = data;
        if(!$scope.ui.images) {
          $scope.ui.images = [];
        }
        for(var i = 0; $scope.ui.images.length > i; i++){
          if ($scope.ui.images[i].main == true){
            $scope.ui.images[i].main = "true";
          }
        }
      } else {
        http_error(data);
      }
    });
    mypromise.error(function(data, status, headers, config) {
      http_error(data);
    });
    function http_error(data){
      if (data.message) {
        alert(data.message);
      } else {
        alert('Unknown error: ' + data);
      }
    }
  };
  $scope.product_getone();

  $scope.images_remove = function(index){
    $scope.ui.images.splice(index, 1);
  };
  $scope.images_add_pic = function(index){
    var b = {
      type:"pic"
    };
    if($scope.ui.images.length == 0){
      b.main = 'true';
    }
    // $scope.ui.images.splice(index + 1,0,b);
    $scope.ui.images.push(b);
  };

  $scope.main_pic_upload = function(images_index){
    var modalInstance = $modal.open({
      templateUrl: 'src/files/modal_upload_file.html',
      controller: 'files_upload_modal',
      size:'lg',
      resolve: {
        injectdata: function(){
          return "pic";
        }
      }
    });

    modalInstance.result.then(function(file_name){
      $scope.ui.images[images_index].file_name = file_name;
      console.log('images name: ' + $scope.ui.images[images_index].name);
      //$scope.main_pic_crop($scope.ui.images[images_index]);
    }, function(){
      //too bad
    });
  };

  $scope.main_pic_crop = function(images){
    var modalInstance = $modal.open({
      templateUrl: 'src/files/modal_pic_crop.html',
      controller: 'pic_crop_modal',
      size:'lg',
      resolve: {
        injectdata: function(){
          var o = {};
          console.log('images name: ' + images.file_name);
          o.file_path = $scope.rs.api_hymne;
          o.file_name = images.file_name;
          return o;
        }
      }
    });

    modalInstance.result.then(function(file_name){
      $scope.ui.images[images_index].file_name = file_name;
    }, function(){
      //too bad
    });
  };

  $scope.pic_select = function(images_index){
    var modalInstance = $modal.open({
      templateUrl: 'src/files/modal_select_file.html',
      controller: 'files_select_modal',
      size:'lg',
      resolve: {
        injectdata: function(){
          return "pic";
        }
      }
    });

    modalInstance.result.then(function(file_name){
      $scope.ui.images[images_index].file_name = file_name;
      console.log('images name: ' + $scope.ui.images[images_index].name);
      $scope.main_pic_crop($scope.ui.images[images_index]);
    }, function(){
      //too bad
    });
  };

  $scope.gallery_pic_crop = function(images_index){
    var modalInstance = $modal.open({
      templateUrl: 'src/files/modal_pic_crop.html',
      controller: 'pic_crop_modal',
      size:'lg',
      resolve: {
        injectdata: function(){
          var o = {};
          o.file_path = $scope.rs.api_hymne;
          o.file_name = $scope.ui.images[images_index].file_name;
          return o;
        }
      }
    });

    modalInstance.result.then(function(file_name){
      $scope.ui.images[images_index].file_name = file_name;
    }, function(){
      //too bad
    });
  };

  $scope.setChoice = function(index) {
    for(var i = 0; $scope.ui.images.length > i; i++){
        $scope.ui.images[i].main = false;
    }
    $scope.ui.images[index].main = 'true';
  };


 function largest_d(){ 
    if(($scope.ui.length > $scope.ui.height) && ($scope.ui.length > $scope.ui.width)) {
      $scope.ui.largest_d = $scope.ui.length;
    }
    if(($scope.ui.length > $scope.ui.height) && ($scope.ui.length < $scope.ui.width)) {
      $scope.ui.largest_d = $scope.ui.width;
    }
    if(($scope.ui.length < $scope.ui.height) && ($scope.ui.height < $scope.ui.width)) {
      $scope.ui.largest_d = $scope.ui.width;
    }
    if(($scope.ui.length < $scope.ui.height) && ($scope.ui.height > $scope.ui.width)) {
      $scope.ui.largest_d = $scope.ui.height;
    }
    setSizeType()
  }
function setSizeType(){
    if($scope.ui.largest_d > 15){
      $scope.ui.mineral_size_type = "Museum (15cm +)";
    }
    if(($scope.ui.largest_d > 10 && $scope.ui.largest_d < 14.99)){
      $scope.ui.mineral_size_type = "Cabinet (10.00 cm to 14.99 cm)";
    }
    if(($scope.ui.largest_d > 7 && $scope.ui.largest_d < 9.99)){
      $scope.ui.mineral_size_type = "Small Cabinet (7.00 cm to 9.99 cm)";
    }
    if(($scope.ui.largest_d > 3 && $scope.ui.largest_d < 6.99)){
      $scope.ui.mineral_size_type = "Miniature (3.00 cm to 6.99 cm)";
    }
    if(($scope.ui.largest_d > 0 && $scope.ui.largest_d < 2.99)){
      $scope.ui.mineral_size_type = "Thumbnail (up to 2.99 cm)";
    }
  }
  $scope.$watch("ui",function(){
        largest_d();
  },true);


  $scope.product_update = function (formvalid) {
    var payload = {};

    for(var i = 0; $scope.ui.images.length > i; i++){
      if ($scope.ui.images[i].main == "true"){
        $scope.ui.images[i].main = true;
      }
    }

    var mypromise =  $http({
      method: 'POST',
      url: $rootScope.api_hymne + '/product_update', //' + $routeParams.id,
      params: { 'foobar': new Date().getTime() },
      data: $scope.ui
    });
    mypromise.success(function(data, status, headers, config) {
      if (status == 200) {
        if(!$scope.ui._id){  
          $location.path('/data_view/' + data._id);
        } else {
          $location.path('/data_view/' + $routeParams.id);
        }
      } else {
        http_error(data);
      }
    });
    mypromise.error(function(data, status, headers, config) {
      http_error(data);
    });
    function http_error(data){
      if (data.message) {
        alert(data.message);
      } else {
        alert('Unknown error: ' + data);
      }
    }
  };

});