main_controllers.controller('data_view_controller', function($http, $scope, $rootScope, $location, $routeParams, modal_service) {
  $rootScope.activemenu = "data_view_controller";

  $scope.rs = $rootScope;
  $scope.ui = {};

  $scope.product_getone = function (formvalid) {
    var payload = {};
    var mypromise =  $http({
      method: 'POST',
      url: $rootScope.api_hymne + '/product_getone/' + $routeParams.id ,
      params: { 'foobar': new Date().getTime() },
      data: payload
    });
    mypromise.success(function(data, status, headers, config) {
      if (status == 200) {
        $scope.ui = data;
        $scope.ui.image_main = $scope.ui.main_pic;
      } else {
        http_error(data);
      }
    });
    mypromise.error(function(data, status, headers, config) {
      http_error(data);
    });
    function http_error(data){
      if (data.message) {
        alert(data.message);
      } else {
        alert('Unknown error: ' + data);
      }
    }
  };
  $scope.product_getone();

  $scope.views_add_one = function () {
    var payload = {};
    payload.item_id = $scope.ui._id;
    payload.person_id = $scope.rs.profile_id;
    var mypromise =  $http({
      method: 'POST',
      url: $rootScope.api_hymne + '/views_add_one',
      params: { 'foobar': new Date().getTime() },
      data: payload
    });
    mypromise.success(function(data, status, headers, config) {
      if (status == 200) {
        $scope.views_return();
      } else {
        http_error(data);
      }
    });
    mypromise.error(function(data, status, headers, config) {
      http_error(data);
    });
    function http_error(data){
      if (data.message) {
        alert(data.message);
      } else {
        alert('Unknown error: ' + data);
      }
    }
  };

  $scope.views_return = function () {
    var payload = {};
    payload.item_id = $routeParams.id;
    payload.person_id = $scope.rs.profile_id;
    var mypromise =  $http({
      method: 'POST',
      url: $rootScope.api_hymne + '/views_return',
      params: { 'foobar': new Date().getTime() },
      data: payload
    });
    mypromise.success(function(data, status, headers, config) {
      if (status == 200) {
        $scope.vi.number_of_views = data;
        console.log($scope.vi);
      } else {
        http_error(data);
      }
    });
    mypromise.error(function(data, status, headers, config) {
      http_error(data);
    });
    function http_error(data){
      if (data.message) {
        alert(data.message);
      } else {
        alert('Unknown error: ' + data);
      }
    }
  };

  $scope.bid = function (index) {
    if(!$scope.rs.session.profile_id){
      modal_service.login();
    } else{
      modal_service.bid(index);
    }
  };

  $scope.add_cart = function (price,image_main,title) {
    if(!$scope.rs.session.profile_id){
      modal_service.login();
    } else{
      modal_service.add_cart(index);
    }
  };

});





