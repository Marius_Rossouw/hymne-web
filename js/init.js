
main_services.factory('init_service', function($rootScope) {
  var service_functions = {};

  service_functions.init_api_uris = function() {
    // $rootScope.api_hymne = 'http://memberbase.net:3888';
    // $rootScope.api_hymne_files = 'http://memberbase.net:3888/files';
    // $rootScope.api_hymne_files100 = 'http://memberbase.net:3888/files100';

    // $rootScope.api_hymne = 'http://hymne_test.memberbase.net/api';
    // $rootScope.api_hymne_files = 'http://hymne_test.memberbase.net/api/files';
    // $rootScope.api_hymnec_files100 = 'http://hymne_test.memberbase.net/api/files100';

    $rootScope.api_hymne = 'http://localhost:3888';
    $rootScope.api_hymne_files = 'http://localhost:3888/files';
    $rootScope.api_hymne_files100 = 'http://localhost:3888/files100';
  };

  service_functions.init_lookup_selectors = function() {
  };

  return service_functions;
});