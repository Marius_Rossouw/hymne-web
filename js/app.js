var am = angular.module('main_app', [
    'ui.bootstrap',
    'main_app.services',
    'main_app.controllers',
    'ngRoute',
    'ngGrid',
    'ngImgCrop',
]);

am.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when("/landing", {templateUrl: "src/new/landing.html", controller: "landing_controller"});

  $routeProvider.when("/data/:id", {templateUrl: "src/new/data_view.html", controller: "data_view_controller"});
  $routeProvider.when("/data_list", {templateUrl: "src/new/data_list.html", controller: "data_list_controller"});
  $routeProvider.when("/data_edit", {templateUrl: "src/new/data_edit.html", controller: "data_edit_controller"});
  $routeProvider.when("/data_edit/:id", {templateUrl: "src/new/data_edit.html", controller: "data_edit_controller"});

  $routeProvider.otherwise({redirectTo: '/landing'});


}]);

var main_services = angular.module('main_app.services', []);
var main_controllers = angular.module('main_app.controllers', []);

am.run(function($rootScope, init_service, lookup_service) {
  console.log("run");
  $rootScope.lookup = {};
  $rootScope.api_hymne = '';

  $rootScope.login = {result : { token:''}};
  $rootScope.session = {};

  init_service.init_api_uris();
  lookup_service.init_lookups();

  // $rootScope.setActiveMenu = function(menu_name){
  //   if (menu_name == 'schools'){
  //     $rootScope.activemenu = menu_name;
  //   } else {
  //     $rootScope.activemenu = menu_name;
  //   }
  //   console.log('Set Menu: ' + menu_name);
  // };
});



am.directive("fileread", [function () {
    return {
        scope: {
            fileread: "="
        },
        link: function (scope, element, attributes) {
            element.bind("change", function (changeEvent) {
                scope.$apply(function () {
                    //scope.fileread = changeEvent.target.files[0];
                    // or all selected files:
                    scope.fileread = changeEvent.target.files;
                });
            });
        }
    };
}]);


