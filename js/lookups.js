main_services.factory("lookup_service", function($rootScope) {
  var service_functions = {};

  service_functions.init_lookups = function() {


    $rootScope.lookup.person_or_business = [
      {"id":"", "desc":"I am a ...."},
      {"id":"Business", "desc":"Business / Organisation"},
      {"id":"Person", "desc":"I am a Person"}
    ];

    $rootScope.lookup.person_business = [
      {"id":"Business", "desc":"I am a Business / Organisation"},
      {"id":"Person", "desc":"I am a Person"}
    ];

    $rootScope.lookup.sort_by = [
      {"id":"New Items", "desc":"New Items"},
      {"id":"Alphabetically", "desc":"Alphabetically"},
      {"id":"Lower Price", "desc":"Lower Price"},
      {"id":"Higher Price", "desc":"Higher Price"},
      {"id":"Most Viewed", "desc":"Most Viewed"}
    ];



    $rootScope.lookup.special_features = [
      {"id":"", "desc":"Select"},
      {"id":"classic locality", "desc":"Classic Locality"},
      {"id":"fluorescent", "desc":"Fluorescent"},
      {"id":"gem crystal", "desc":"Gem Crystal"},
      {"id":"new discovery", "desc":"New Discovery"},
      {"id":"rare mineral", "desc":"Rare Mineral"},
      {"id":"radioactive", "desc":"Radioactive"},
      {"id":"pseudomorph", "desc":"Pseudomorph"}
    ];

    $rootScope.lookup.type_sale = [
      {"id":"Undecided", "desc":"Undecided"},
      {"id":"Part of Auction", "desc":"Part of Auction"},
      {"id":"Shop Item", "desc":"Shop Item"},
    ];

    $rootScope.lookup.type = [
      {"id":"Minerals", "desc":"Minerals"},
      {"id":"Medals & Ribbons", "desc":"Medals & Ribbons"},
      {"id":"Coins & Notes", "desc":"Coins & Notes"},
      {"id":"Stamps", "desc":"Stamps"},
    ];

    $rootScope.lookup.mineral_size_type = [
      {"id":"", "desc":"Select"},
      {"id":"Wholesale", "desc":"Wholesale"},
      {"id":"Museum (15cm +)", "desc":"Museum (15cm +)"},
      {"id":"Cabinet (10.00 cm to 14.99 cm)", "desc":"Cabinet (10.00 cm to 14.99 cm)"},
      {"id":"Small Cabinet (7.00 cm to 9.99 cm)", "desc":"Small Cabinet (7.00 cm to 9.99 cm)"},
      {"id":"Miniature (3.00 cm to 6.99 cm)", "desc":"Miniature (3.00 cm to 6.99 cm)"},
      {"id":"Thumbnail (up to 2.99 cm)", "desc":"Thumbnail (up to 2.99 cm)"},
    ];

    $rootScope.lookup.categories = [
      {"id":"", "desc":"Select"},
      {"id":"1", "desc":"N Chwaning / Wessels / Hotazel Mines"},
      {"id":"2", "desc":"Rest of Southern African Mines"},
      {"id":"3", "desc":"Brandberg, Erongo & Okorusu Namibia"},
      {"id":"4", "desc":"Fine Selection of World Minerals"},
      {"id":"5", "desc":"Southern Africa & World Mineral Books"},
      {"id":"6", "desc":"Okandawasi Koakoland Mines Namibia"},
      {"id":"7", "desc":"Rosh Pinah & Skorpion Mine Namibia"},
      {"id":"8", "desc":"Orange River Riemvasmaak"},
      {"id":"9", "desc":"Wholesale Lots to the Trade"},
      {"id":"10", "desc":"The Colin R Owen Collection"},
      {"id":"20", "desc":"Sugilite from Wessels Mine"},
      {"id":"21", "desc":"Tsumeb / Kombat / Berg Aukas Mines"},
      {"id":"22", "desc":"Pictures of Mines in Southern Africa"},
      {"id":"23", "desc":"Meteorites from Namibia & Central Africa"},
      {"id":"24", "desc":"Musina, Zimbabwe, Zamibia & DRC"}
    ];

    $rootScope.lookup.special_characteristics = [
      {"id":"", "desc":"-None-"},
      {"id":"Fluorescent", "desc":"Fluorescent"},
      {"id":"Gem Crystal", "desc":"Gem Crystal"},
      {"id":"New Discovery", "desc":"New Discovery"},
      {"id":"New Mineral type", "desc":"New Mineral type"},
      {"id":"Pseudomorph", "desc":"Pseudomorph"},
      {"id":"Radioactive", "desc":"Radioactive"},
      {"id":"Rare Mineral", "desc":"Rare Mineral"},
      {"id":"Type Locality", "desc":"Type Locality"},
      {"id":"With Antique Label", "desc":"With Antique Label"}
    ];


    $rootScope.lookup.special_class = [
      {"id":"", "desc":"-None-"},
      {"id":"Single Crystal", "desc":"Single Crystal"},
      {"id":"Crystal Group/Cluster", "desc":"Crystal Group/Cluster"},
      {"id":"Crystals on Matrix", "desc":"Crystals on Matrix"},
      {"id":"Massive/Crstalline", "desc":"Massive/Cristalline"},
      {"id":"Combination", "desc":"Combination"},
      {"id":"Mixed Specimens", "desc":"Mixed Specimens"},
      {"id":"Artefact", "desc":"Artefact"}
    ];

    $rootScope.lookup.item_type = [
      {"id":"Mineral", "desc":"Mineral"},
      {"id":"Millateria", "desc":"Millateria"},
      {"id":"Coins", "desc":"Coins"},
      {"id":"Stamps", "desc":"Stamps"}
    ];

    $rootScope.lookup.collection_type = [
      {"id":"", "desc":"Select"},
      {"id":"Shop", "desc":"Shop"},
      {"id":"Auction", "desc":"Auction"},
      {"id":"Private Collection", "desc":"Private Collection"}
    ];

    $rootScope.lookup.lookup_numbers4 = [];
    for (var i=0; i <= 4; i++) {
      $rootScope.lookup.lookup_numbers4.push({"id": i, "desc": i.toString()});
    }


    $rootScope.lookup.lookup_date_day = ['Day','01','02','03','04','05','06','07','08','09'];
    for (var i=10; i <= 31; i++) {
      $rootScope.lookup.lookup_date_day.push(i.toString());
    }
    $rootScope.lookup.lookup_date_month = ['Month','01','02','03','04','05','06','07','08','09','10','11','12'];
    $rootScope.lookup.lookup_date_year = [
      "Year"
    ];
    for (var i=2014; i >= 1930; i--) {
      $rootScope.lookup.lookup_date_year.push(i.toString());  //{"id":0, "desc":"Select"} {"id": i, "desc": i.toString()}
    }
    $rootScope.lookup.lookup_date_year_card = [
      "Year"
    ];
    for (var i=2025; i >= 2014; i--) {
      $rootScope.lookup.lookup_date_year_card.push(i.toString());  //{"id":0, "desc":"Select"} {"id": i, "desc": i.toString()}
    }
  };

  return service_functions;
});



main_services.factory("modal_service", function($rootScope, $modal) {
  var service_functions = {};

  service_functions.login = function(callback){
    var modalInstance = $modal.open({
      templateUrl: 'src/session/login.html',
      controller: 'login_controller',
      size:'lg',
      resolve: {
        injectdata: function(){
          return "something";
        }
      }
    });

    modalInstance.result.then(function(file_name){
      //$scope.cep.product = file_name;
      //$scope(.ui_get_person_button();
      callback(null,{});
    }, function(){
      //too bad
      callback(null,{});
    });
  };


  service_functions.all_in_one = function(callback){
    var modalInstance = $modal.open({
      templateUrl: 'src/session/all_in_one.html',
      controller: 'login_controller',
      size:'lg',
      resolve: {
        injectdata: function(){
          return "something";
        }
      }
    });

    modalInstance.result.then(function(file_name){
      //$scope.cep.product = file_name;
      //$scope(.ui_get_person_button();
      callback(null,{});
    }, function(){
      //too bad
      callback(null,{});
    });
  };



  service_functions.bid = function(callback){
    var modalInstance = $modal.open({
      templateUrl: 'src/auction/bid.html',
      controller: 'bid_controller',
      size:'lg',
      resolve: {
        injectdata: function(){
          return "something";
        }
      }
    });

    modalInstance.result.then(function(file_name){
      //$scope.cep.product = file_name;
      //$scope(.ui_get_person_button();
      callback(null,{});
    }, function(){
      //too bad
      callback(null,{});
    });
  };


  service_functions.add_cart = function(callback){
    var modalInstance = $modal.open({
      templateUrl: 'src/shop/shop_add_cart.html',
      controller: 'shop_add_to_cart_controller',
      size:'lg',
      resolve: {
        injectdata: function(){
          return "something";
        }
      }
    });

    modalInstance.result.then(function(file_name){
      //$scope.cep.product = file_name;
      //$scope(.ui_get_person_button();
      callback(null,{});
    }, function(){
      //too bad
      callback(null,{});
    });
  };

  return service_functions;
});



