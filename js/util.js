function navbar_remove_all_active() {
  $('#content_navbar li').each(function() {
    $("#"+this.id).removeClass("active");
  });
}

function checkTime(i) {
  if (i < 10) {
      i = "0" + i;
  }
  return i;
}



function startTime() {
  document.getElementById('time').innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + moment().format('HH:mm:ss');
  t = setTimeout(function () {
      startTime();
  }, 500);
}
//startTime();




function startTime2() {
  document.getElementById('time2').innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + moment().format('DD MMM YYYY');
  t = setTimeout(function () {
      startTime();
  }, 500);
}
//startTime2();


  function calc_date(year, month, day, assign_dob, assign_age){
    if (!year || !month || !day) {
      assign_dob('');
      assign_age(0);
      return;
    }
    if (year == 'Year' || month == 'Month' || day == 'Day') {
      assign_dob('');
      assign_age(0);
      return;
    }
    var mdob = moment(year + '-' + month + '-' + day, "YYYY-MM-DD");
    if (!mdob.isValid()) {
      assign_dob('');
      assign_age(0);
      return;
    }
    assign_dob(mdob.format("YYYY-MM-DD"));
    assign_age(calc_moment_age(mdob));
  }

  function calc_moment_age(dob) {
    var today = moment();
    var age = today.year() - dob.year();
    if (today.month() < dob.month()) {
      age = age - 1;
      return age;
    }
    if ((today.month() == dob.month()) && (today.date() < dob.date())) {
      age = age - 1;
      return age;
    }
    return age;
  }

  function isInt(value) {
    return !isNaN(value) && (function(x) { return (x | 0) === x; })(parseFloat(value));
  }

