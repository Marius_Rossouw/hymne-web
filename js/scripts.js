
/*Document Ready*////////////////////////////////////////////////////////////////////////////////////////////////////////////////
jQuery(document).ready(function($) {
	'use strict';
	
	/*Global Variables
	*******************************************/
	/// Header / Navigation Variables------------------------------------
	var $header = $('.header');
	var $headerToolbar = $('.header-toolbar');
	var $stickyHeader = $('.header.sticky');
	var $scrollHeader = $('.header.scroller');
	var $transpHeader = $('.header.transparent');

	var $menuItem = $('.menu ul li');
	var $naviToggle = $('#nav-toggle');
	var $exitOffCanv = $('.exit-off-canvas');

	
	/// Hero Units Variables--------------------------------------------
	var $heroFs = $('.hero-static.fullscreen');
	var $heroFsInner = $('.hero-static.fullscreen .inner');
	var $heroParallax = $('.hero-parallax')
	/// ----------------------------------------------------------------
	
	/// Forms Variables-------------------------------------------------
	var $contForm = $('.contact-form');
	var $qcForm = $('.quick-contact');
	var $scrollTopBtn = $('#scrollTop-btn');

	/// ----------------------------------------------------------------
	
	///Interactive Widgets Variables------------------------------------
	var $harmonic = $('.harmonic .item');
	var $tooltip = $('.tooltipped');
	var $percentChart = $('.percent-chart');
	var $sidebarBtn = $('.sidebar-button');
	var $shareBar = $('.share-modal .bar');
	var $infoClose = $('.info-string .close');
	/// ----------------------------------------------------------------
	
	/// Shop Variables------------------------------------------
	var $shareBtn = $('.item footer .tools .share-btn');
	/// ----------------------------------------------------------------
	
	/// User Account Variables------------------------------------------
	var $editPass = $('.account-settings .pass-block .edit-btn');
	var $closePass = $('.account-settings .pass-block .close');
	/// ----------------------------------------------------------------
	
	
	///////////////////////////////////////////////////////////////////////
	/////////////////////  Fullscreen Hero Image  /////////////////////////
	//////////////////////////////////////////////////////////////////////
	
	$(window).on('load', function(){
		$heroFs.after('<div class="holder"></div>');
		$heroFs.css('min-height', $(window).height());
		$heroFsInner.css('min-height', $(window).height());
		$('.holder').css('min-height', $(window).height());
		if($stickyHeader.length > 0) {
			$('.holder').css('min-height', $(window).height()-$stickyHeader.height());
		}
	});
	$(window).on('resize', function(){
		$heroFs.css('min-height', $(window).height());
		$heroFsInner.css('min-height', $(window).height());
		$('.holder').css('min-height', $(window).height());
		if($stickyHeader.length > 0) {
			$('.holder').css('min-height', $(window).height()-$stickyHeader.height());
		}
	});

	///////////////////////////////////////////////////////////////////////
	///////////////////  Header / Navigation  /////////////////////////////
	//////////////////////////////////////////////////////////////////////
	
	/*Sticky Header
	*******************************************/
	$(window).on('load', function(){
		$stickyHeader.waypoint('sticky');
		$scrollHeader.waypoint('sticky');
	});
	
	/*Transparent Header
	*******************************************/
	$(window).on('load', function(){
		if($transpHeader.length > 0) {
			var $logoAlt = $transpHeader.find('.logo > img').data('logo-alt');
			$transpHeader.find('.logo > img').attr('src', $logoAlt);
		}
	});
	$(window).on('scroll', function(){
		var $logoAlt = $transpHeader.find('.logo > img').data('logo-alt');
		var $logoDefault = $transpHeader.find('.logo > img').data('logo-default');
		if($(window).scrollTop() > $(window).height()) {
			$transpHeader.addClass('opaque');
			$transpHeader.find('.logo > img').attr('src', $logoDefault);
		} else {
			$transpHeader.removeClass('opaque');
			$transpHeader.find('.logo > img').attr('src', $logoAlt);
		}
	});
		
	
	/*Navi Toggle Animation
	*******************************************/
	$naviToggle.click(function(){
		$(this).toggleClass('active');
		$header.find('.inner').toggleClass('no-shadow');
	});
	$exitOffCanv.click(function(){
		$naviToggle.removeClass('active');
		$header.find('.inner').removeClass('no-shadow');
	});
	
	/*Foundation Off-Canvas / Intercharge (for responsive images)
	***************************************************************************************/
	$(document).foundation({
		offcanvas : {
			open_method: 'move', // Sets method in which offcanvas opens, can also be 'overlap'
			close_on_click : true
		}
	});
	

	
	///////////////////////////////////////////////////////////////////////
	///////////  Parallax Backgrounds / OnScroll Animations  //////////////
	//////////////////////////////////////////////////////////////////////

	
	///////////////////////////////////////////////////////////////////////
	/////////////////////////  Parallax Hero  ////////////////////////////
	//////////////////////////////////////////////////////////////////////
	
	$heroParallax.parallax();
	
	///////////////////////////////////////////////////////////////////////
	///////////////////////  Custom Widgets  /////////////////////////////
	//////////////////////////////////////////////////////////////////////
	
	/*Harmonic Widget
	*********************************************/
	$harmonic.hover(function(){
		$harmonic.addClass('collapsed');
		$(this).removeClass('collapsed').addClass('expanded');
	}, function(){
		$harmonic.removeClass('collapsed').removeClass('expanded');
	});
	
	/*Share Bars Animation + Mail (Share Modal)
	*********************************************/
	$shareBar.hover(function(){
		$shareBar.addClass('collapsed');
		$(this).removeClass('collapsed').addClass('expanded');
	}, function(){
		$shareBar.removeClass('collapsed').removeClass('expanded');
	});
	
	/*Animated Digits Widget
	************************************************************/
	//Function
	var countNumb = (function() {
		var that = {};
		that.init = function() {

				$(".animated-digits .digit").each(function() {
						var dataNumber = $(this).attr('data-number');

						$(this).numerator({
								easing: 'swing', // easing options.
								duration: 2500, // the length of the animation.
								delimiter: '.',
								rounding: 0, // decimal places.
								toValue: dataNumber // animate to this value.
						});
				});
		};

		return that;

	})();
	
	//Initialisation
	$('.digits').waypoint(function(direction) {
			countNumb.init() + 'down';
	}, {
			offset: '65%',
			triggerOnce: true
	});
	

	/*Slide Up/Down Home Events
	************************************************************/
	$('#slide-up-toggle').click(function(){
		$('.slide-up').toggleClass('open');
	});
	
	/*Dismissing / Closing Elements
	************************************************************/
	//Information Strip on top of header
	$infoClose.click(function(){
		var $target = $(this).parent().parent();
		$target.fadeOut(300, function(){
			$target.remove();
			$.waypoints('refresh');
		});
	});
	

	
	/*Animated Percents Chart (Skills)
	************************************************************/
	var $barColor = $percentChart.data('bar-color');
	$percentChart.waypoint(function(direction) {
		$percentChart.easyPieChart({
			animate: 2000,
			easing: 'easeOutBounce',
			barColor: $barColor,
			trackColor: '#e1e4e6',
			scaleColor: '#797979', 
			size: 190,
			lineWidth: 7,
			onStep: function(from, to, percent) {
				$(this.el).find('.percent').text(Math.round(percent));
			}
		}) + 'down';
			//countNumb.init() + 'down';
	}, {
			offset: '85%',
			triggerOnce: true
	});

	
	/*UI Slider
	*******************************************/
	var $minVal = parseInt($('.ui-slider').attr('data-min-val'));
	var $maxVal = parseInt($('.ui-slider').attr('data-max-val'));
	var $start = parseInt($('.ui-slider').attr('data-start'));
	var $step = parseInt($('.ui-slider').attr('data-step'));
	
	if($('#ui-slider').length>0){
		$('#ui-slider').noUiSlider({
			start: [$start],
			connect: "lower",
			step: $step,
			range: {
				'min': $minVal,
				'max': $maxVal
			},
			format: wNumb({
				decimals: 0,
				thousand: ' ',
				postfix: ' $'
			})
		});
		$("#ui-slider").Link('lower').to('-inline-<div class="tool-tip"></div>', function ( value ) {
			// The tooltip HTML is 'this', so additional
			// markup can be inserted here.
			$(this).html(
				'<span>' + value + '</span>'
			);
		});
	}
	
	/*Tooltips
	*******************************************/
	$tooltip.tooltip();

	/*Share Button
	*******************************************/
	$shareBtn.click(function(e){
		$(this).toggleClass('active');	
		$(this).parent().parent().parent().find('.share-opts').toggleClass('show');	
		$(this).parent().parent().parent().find('.description').toggleClass('hide');
		e.preventDefault();	
	});
	


	
	///////////////////////////////////////////////////////////////////////
	////////////////////////////  User Account  //////////////////////////
	//////////////////////////////////////////////////////////////////////
	
	/*Password Change Box
	*******************************************/
	$editPass.click(function(){
		$('.pass-block').addClass('expanded');
	});
	
	$closePass.click(function(){
		$('.pass-block').removeClass('expanded');
	});
	
	///////////////////////////////////////////////////////////////////////
	///////////////////  Gallery Grids + Filtering  //////////////////////
	//////////////////////////////////////////////////////////////////////

	

	
	// filter items when filter link is clicked
	$('.filters a').click(function(){
		var selector = $(this).attr('data-filter');
		$gallGridMassonry.isotope({ 
			filter: selector
		});
		return false;
	});
	

	///////////////////////////////////////////////////////////////////////
	/////////////////////////  Google Maps  //////////////////////////////
	//////////////////////////////////////////////////////////////////////
	var geocoder;
	var map;
	var query = $('.google-map').data('location');
	var zoom = $('.google-map').data('zoom');
	
	function initialize() {
		
		geocoder = new google.maps.Geocoder();
		var latlng = new google.maps.LatLng(-34.397, 150.644);
		
			var mapOptions = {
				center: latlng,
				zoom: zoom,
				scrollwheel: false,
				disableDefaultUI: true,
				styles: [{"featureType":"landscape","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"poi","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"stylers":[{"hue":"#00aaff"},{"saturation":-100},{"gamma":2.15},{"lightness":12}]},{"featureType":"road","elementType":"labels.text.fill","stylers":[{"visibility":"on"},{"lightness":24}]},{"featureType":"road","elementType":"geometry","stylers":[{"lightness":57}]}]
			};
			
			map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
			
			codeAddress();
	}
	
	function codeAddress() {
		var image = 'img/map-marker.png';
		var address = query;
		geocoder.geocode( { 'address': address}, function(results, status) {
			if (status == google.maps.GeocoderStatus.OK) {
				map.setCenter(results[0].geometry.location);
				var marker = new google.maps.Marker({
						map: map,
						position: results[0].geometry.location,
						icon: image,
						title: 'My Kedavra Office'
				});
			} else {
				alert('Geocode was not successful for the following reason: ' + status);
			}
		});
	}
	
	if($('#map-canvas').length > 0) {
		google.maps.event.addDomListener(window, 'load', initialize);
	}
	

	
	///////////////////////////////////////////////////////////////////////
	///////////////////  User Account Interactions  ///////////////////////
	//////////////////////////////////////////////////////////////////////
	
	/*Hiding User Image in User Account Page - For Demo Only
	**********************************************************************/
	$('.user-avatar .delete').click(function(){
		$(this).hide();
		$(this).parent().find('.user-img').addClass('removed');
	});
	
	/*Adding / Canceling Addresses
	**********************************************************************/
	$('#adress-settings .add-address-btn').click(function(e){
		var $target = $(this).attr('href');
		$('#adress-settings').addClass('hidden');
		$('.hidden-form').removeClass('active');
		$($target).addClass('active');
		e.preventDefault();
	});
	$('.cancel-address-btn').click(function(e){
		$('#adress-settings').removeClass('hidden');
		$('.hidden-form').removeClass('active');
		e.preventDefault();
	});
	
	///////////////////////////////////////////////////////////////////////
	///////////////////  Interactive Radar Chart  /////////////////////////
	//////////////////////////////////////////////////////////////////////
	
	var fillColor = $('.chart').data('fill');
	var lineColor = $('.chart').data('lines');
	var radarChartData = {
		labels: ["Administrative", "Bankruptcy", "Employment/Labor", "Immigration", "Transactions", "Divorces", "Criminal Law"],
		datasets: [
			{
				label: "First dataset",
				fillColor: "rgba(220,220,220,0.2)",
				strokeColor: "rgba(220,220,220,1)",
				pointColor: "rgba(220,220,220,1)",
				pointStrokeColor: "#fff",
				pointHighlightFill: "#fff",
				pointHighlightStroke: "rgba(220,220,220,1)",
				data: [68,50,92,78,46,100,40]
			},
			{
				label: "Second dataset",
				fillColor: fillColor,
				strokeColor: lineColor,
				pointColor: lineColor,
				pointStrokeColor: "#fff",
				pointHighlightFill: "#fff",
				pointHighlightStroke: lineColor,
				data: [28,68,40,19,96,27,100]
			}
		]
	};

	window.onload = function(){
		if($('#chart').length > 0){
			window.myRadar = new Chart(document.getElementById("chart").getContext("2d")).Radar(radarChartData, {
				responsive: true
			});
		}
	}


		
		
	///////////////////////////////////////////////////////////////////////
	/////////  INTERNAL ANCHOR LINKS SCROLLING (NAVIGATION)  //////////////
	//////////////////////////////////////////////////////////////////////
	
	$(".scroll").click(function(event){		
		var $elemOffsetTop = $(this).data('offset-top');
		$('html, body').animate({scrollTop:$(this.hash).offset().top-$elemOffsetTop}, 1000, 'easeOutExpo');
		$naviToggle.removeClass('active');
		event.preventDefault();
	});
	$('.scrollup').click(function(e){
		e.preventDefault();
		$('html, body').animate({scrollTop : 0}, {duration: 700, easing:'easeOutExpo'});
		$naviToggle.removeClass('active');
	});

	
	$(window).scroll(function(){
		if ($(this).scrollTop() > 500) {
			$('#scroll-top').addClass('visible');
		} else {
			$('#scroll-top').removeClass('visible');
		}
	});
	
	//SCROLL-SPY
	// Cache selectors
	var lastId,
		topMenu = $(".scroll-menu"),
		topMenuHeight = topMenu.outerHeight(),
		// All list items
		menuItems = topMenu.find("a"),
		// Anchors corresponding to menu items
		scrollItems = menuItems.map(function(){
		  var item = $($(this).attr("href"));
		  if (item.length) { return item; }
		});
	
	// Bind to scroll
	$(window).scroll(function(){
	   // Get container scroll position
	   var fromTop = $(this).scrollTop()+topMenuHeight+200;
	   
	   // Get id of current scroll item
	   var cur = scrollItems.map(function(){
		 if ($(this).offset().top < fromTop)
		   return this;
	   });
	   // Get the id of the current element
	   cur = cur[cur.length-1];
	   var id = cur && cur.length ? cur[0].id : "";
	   
	   if (lastId !== id) {
		   lastId = id;
		   // Set/remove active class
		   menuItems
			 .parent().removeClass("active")
			 .end().filter("[href=#"+id+"]").parent().addClass("active");
	   }
	});
	////////////////////////////////////////////////////////////////////
	
	///////////////////////////////////////////////////////////////////////
	///////////////////  Scroll to Sidebar Button  ////////////////////////
	//////////////////////////////////////////////////////////////////////
	
	$sidebarBtn.click(function(event){		
		event.preventDefault();
		$('html, body').animate({scrollTop:$(this.hash).offset().top+2}, 800, 'easeOutExpo');
	});
	
	///////////////////////////////////////////////////////////////////////
	///////////////////////  Sticky Buttons  /////////////////////////////
	//////////////////////////////////////////////////////////////////////
	
	//Scroll to Top Button
	$(window).scroll(function(){
		if ($(this).scrollTop() > 500) {
			$scrollTopBtn.parent().addClass('scrolled');
		} else {
			$scrollTopBtn.parent().removeClass('scrolled');
		}
	});
	$scrollTopBtn.click(function(){
		$('html, body').animate({scrollTop : 0}, {duration: 700, easing:'easeOutExpo'});
	});
	

});/*Document Ready End*//////////////////////////////////////////////////////////////////////////
	
/*Back Function: Manipulating the browser history
*************************************************/
function goBack() {
	window.history.back()
}
