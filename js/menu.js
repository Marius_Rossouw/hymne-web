main_controllers.controller('menu_controller', function($scope, $rootScope, $location, $modal, modal_service) {
  $scope.rs = $rootScope;

  $scope.logout = function () {
    $rootScope.session.authed = {};
    $rootScope.session.token = {};
    $rootScope.session.profile_name = {};
    $rootScope.session.profile_id = null;
    $rootScope.session.role_admin = {};
    $rootScope.session.role_merchant = {};
    $rootScope.session.role_client = {};
    $rootScope.session.avatar = {};
    $rootScope.session.email = {};
    $rootScope.session.contact_number = {};
    $rootScope.session.first_name = {};
    $rootScope.session.last_name = {};

    $location.path('/landing');
  };

  $scope.landing_button = function () {
    $location.path('/landing');
  };

  $scope.login_button = function () {
    var modalInstance = $modal.open({
      templateUrl: 'src/session/login.html',
      controller: 'login_controller',
      size:'lg',
      resolve: {
        injectdata: function(){
          return "something";
        }
      }
    });
    modalInstance.result.then(function(file_name){
      $location.path('/landing');
    }, function(){
      //too bad
    });
  };

  $scope.register_button = function () {
    var modalInstance = $modal.open({
      templateUrl: 'src/session/login.html',
      controller: 'login_controller',
      size:'lg',
      resolve: {
        injectdata: function(){
          return "something";
        }
      }
    });
    modalInstance.result.then(function(file_name){
      $location.path('/landing');
    }, function(){
      //too bad
    });
  };

  $scope.data_list_button = function () {
    $location.path('/data_list');
  };
  $scope.data_add_button = function () {
    $location.path('/data_edit/new');
  };

  $scope.all_in_one_button = function () {
   if(!$scope.rs.session.profile_id){
    modal_service.all_in_one();
    } else {
      return;
    }
  };

});